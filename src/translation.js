import { gettext as _ } from "resource:///org/gnome/shell/extensions/extension.js";

/*
the next lines are only here to be caugth by translation
this is is not packed with the extension or installed - only purpose is to catch the strings and
add them to po file by "xgettext --from-code=UTF-8 --output=locale/Shortcuts.pot src/*.js src/schemas/*.xml"
*/
const strFix1 = _("Window Navigation");
const strFix2 = _("Alt + Tab");
const strFix3 = _("Switches between windows");
const strFix4 = _("Super");
const strFix5 = _("Enter overview");
const strFix6 = _("Super + Left/Right");
const strFix7 = _("Tile windows to the left or right");
const strFix8 = _("Super + Up");
const strFix9 = _("Maximize window");
const strFix10 = _("Alt + F4");
const strFix11 = _("Close window");
const strFix12 = _("Super + Mouse Drag");
const strFix13 = _("Move window");
const strFix14 = _("F11");
const strFix15 = _("Make full screen/restore");
const strFix16 = _("Super + H");
const strFix17 = _("Minimize window");
const strFix18 = _("Super + Page Down");
const strFix19 = _("Switch to next workspace");
const strFix20 = _("Super + Page Up");
const strFix21 = _("Switch to previous workspace");
const strFix22 = _("Super + Home");
const strFix23 = _("Switch to first workspace");
const strFix24 = _("Super + End");
const strFix25 = _("Switch to last workspace");
const strFix26 = _("Super + Tab");
const strFix27 = _("Switch to next application");
const strFix28 = _("Run and Close Applications");
const strFix29 = _("Alt + F2");
const strFix30 = _("Run dialog");
const strFix31 = _("Super + 1,2,3,...");
const strFix32 = _("Run app by position in the launcher");
const strFix33 = _("Super + T");
const strFix34 = _("Open terminal");
const strFix35 = _("Super + Q");
const strFix36 = _("Quit application");
const strFix37 = _("System Shortcuts");
const strFix38 = _("Super + Space");
const strFix39 = _("Switch keyboard layout");
const strFix40 = _("Ctrl + Alt + Del");
const strFix41 = _("Opens the shutdown menu");
const strFix42 = _("Super + L");
const strFix43 = _("Locks the screen");
const strFix44 = _("Alt + F2, r");
const strFix45 = _("Restart GNOME Shell");
const strFix46 = _("Alt + Super + 8");
const strFix47 = _("Toggle zoom");
const strFix48 = _("Alt + Super + S");
const strFix49 = _("Toggle screen reader");
const strFix50 = _("Application Shortcuts");
const strFix51 = _("Ctrl + W");
const strFix52 = _("Close tab in tab enabled apps");
const strFix53 = _("Ctrl + T");
const strFix54 = _("Opens new tab in tab enabled apps");
const strFix55 = _("Ctrl + Shift + T");
const strFix56 = _("Reopen last closed tab in web browser");
const strFix57 = _("F5");
const strFix58 = _("Evince (document viewer) presentation mode");
const strFix59 = _("F9");
const strFix60 = _("LibreOffice presentation mode");
const strFix61 = _("Screenshot Shortcuts");
const strFix62 = _("Alt + Print");
const strFix63 = _("Take a screenshot");
const strFix64 = _("Shift + Print");
const strFix65 = _("Take a screenshot of a region");
const strFix66 = _("Ctrl + Print");
const strFix67 = _("Take a screenshot and paste in a clipboard");
const strFix68 = _("Ctrl + Alt + Shift + R");
const strFix69 = _("Video capture desktop");

console.log(strFix1 + strFix2 + strFix3 + strFix4 + strFix5 + strFix6 + strFix7 +strFix8 + strFix9 +strFix10)
console.log(strFix11 + strFix12 + strFix13 + strFix14 + strFix15 + strFix16 + strFix17 +strFix18 + strFix19 +strFix20)
console.log(strFix21 + strFix22 + strFix23 + strFix24 + strFix25 + strFix26 + strFix27 +strFix28 + strFix29 +strFix30)
console.log(strFix31 + strFix32 + strFix33 + strFix34 + strFix35 + strFix36 + strFix37 +strFix38 + strFix39 +strFix40)
console.log(strFix41 + strFix42 + strFix43 + strFix44 + strFix45 + strFix46 + strFix47 +strFix48 + strFix49 +strFix50)
console.log(strFix51 + strFix52 + strFix53 + strFix54 + strFix55 + strFix56 + strFix57 +strFix58 + strFix59 +strFix60)
console.log(strFix61 + strFix62 + strFix63 + strFix64 + strFix65 + strFix66 + strFix67 +strFix68 + strFix69)